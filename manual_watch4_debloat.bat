@REM ## ###########
@REM #
@REM # Samsung Bloatware to Disable
@REM # thanks @ https://forum.xda-developers.com/t/debloat-galaxy-watch-4.4324147/
@REM #
@REM #####

@REM # Samsung Account
adb shell pm disable-user --user 0 com.samsung.android.watch.watchface.myphoto
adb shell pm disable-user --user 0 com.samsung.android.watch.watchface.mystyle
adb shell pm disable-user --user 0 com.samsung.android.watch.compass
adb shell pm disable-user --user 0 com.samsung.android.watch.weather
adb shell pm disable-user --user 0 com.samsung.android.messaging
adb shell pm disable-user --user 0 com.samsung.android.watch.watchface.basicclock
adb shell pm disable-user --user 0 com.samsung.android.bixby.agent
adb shell pm disable-user --user 0 com.samsung.android.watch.watchface.simpleclassic
adb shell pm disable-user --user 0 com.samsung.android.watch.watchface.dualwatch
adb shell pm disable-user --user 0 com.samsung.android.samsungpay.gear
adb shell pm disable-user --user 0 com.samsung.android.app.reminder
adb shell pm disable-user --user 0 com.samsung.android.watch.watchface.together
adb shell pm disable-user --user 0 com.samsung.android.watch.watchface.typography
adb shell pm disable-user --user 0 com.samsung.android.watch.watchface.large
adb shell pm disable-user --user 0 com.samsung.android.watch.watchface.animal
adb shell pm disable-user --user 0 com.google.android.apps.maps
adb shell pm disable-user --user 0 com.samsung.android.watch.watchface.aremoji
adb shell pm disable-user --user 0 com.samsung.android.watch.watchface.analogmodular
adb shell pm disable-user --user 0 com.samsung.android.watch.watchface.emergency
adb shell pm disable-user --user 0 com.samsung.android.watch.watchface.simpleanalogue
adb shell pm disable-user --user 0 com.google.android.apps.wearable.retailattractloop
adb shell pm disable-user --user 0 com.samsung.android.bixby.wakeup
adb shell pm disable-user --user 0 com.samsung.android.watch.watchface.bitmoji
adb shell pm disable-user --user 0 com.android.providers.userdictionary
adb shell pm disable-user --user 0 com.samsung.android.watch.watchface.superfiction
adb shell pm disable-user --user 0 com.samsung.android.watch.worldclock
adb shell pm disable-user --user 0 com.samsung.android.watch.watchface.endangeredanimal

adb shell pm disable-user --user 0 com.samsung.android.shealthmonitor

adb shell pm disable-user --user 0 com.samsung.android.watch.watchface.analoguefont
adb shell pm disable-user --user 0 com.samsung.android.watch.worldclock
adb shell pm disable-user --user 0 com.samsung.android.watch.cameracontroller
adb shell pm disable-user --user 0 com.samsung.android.watch.watchface.simplecomplication
adb shell pm disable-user --user 0 com.samsung.android.watch.watchface.digitalfont
adb shell pm disable-user --user 0 com.samsung.android.watch.watchface.digitalmodular
adb shell pm disable-user --user 0 com.google.android.marvin.talkback
adb shell pm disable-user --user 0 com.samsung.android.wearable.music

adb shell pm disable-user --user 0 com.samsung.android.watch.watchface.weather
adb shell pm disable-user --user 0 com.samsung.android.watch.watchface.healthmodular
adb shell pm disable-user --user 0 com.samsung.android.watch.watchface.livewallpaper

adb shell pm disable-user --user 0 com.google.android.wearable.assistant

adb shell pm disable-user --user 0 com.samsung.android.watch.watchface.basicdashboard
adb shell pm disable-user --user 0 com.samsung.android.video.wearable

@REM ## ###########
@REM #
@REM # Ask to Reboot
@REM #
@REM #####
setlocal
:PROMPT
SET /P REBOOTNOW=Reboot now (Y/[N])?
IF /I "%REBOOTNOW%" NEQ "Y" GOTO END
adb reboot
:END
