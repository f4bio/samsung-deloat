@REM ## ###########
@REM #
@REM # Samsung Bloatware to Enable
@REM #
@REM #####

@REM # Bixby
adb shell pm enable com.samsung.android.bixby.wakeup
adb shell pm enable com.samsung.android.app.spage
adb shell pm enable com.samsung.android.app.routines
adb shell pm enable com.samsung.android.bixby.service
adb shell pm enable com.samsung.android.visionintelligence
adb shell pm enable com.samsung.android.bixby.agent
adb shell pm enable com.samsung.android.bixby.agent.dummy
adb shell pm enable com.samsung.android.bixbyvision.framework
adb shell pm enable com.samsung.android.app.settings.bixby
adb shell pm enable com.samsung.systemui.bixby
adb shell pm enable com.samsung.systemui.bixby2
@REM # Samsung Account
adb shell pm enable com.osp.app.signin
@REM # Samsung Auto fill
@REM # Samsung Authentication
adb shell pm enable com.samsung.android.authfw
@REM # Samsung Pass
adb shell pm enable com.samsung.android.samsungpass
adb shell pm enable com.samsung.android.samsungpassautofill
@REM # Samsung Pay
adb shell pm enable com.samsung.android.spay
adb shell pm enable com.samsung.android.spayfw
adb shell pm enable com.sec.android.app.billing
@REM # Samsung Email
adb shell pm enable com.samsung.android.email.provider
adb shell pm enable com.wsomacp
@REM # Samsung Internet
adb shell pm enable com.sec.android.app.sbrowser
@REM # Samsung Blockchain Wallet
adb shell pm enable com.samsung.android.coldwalletservice
@REM # Samsung Text-to-Speech
adb shell pm enable com.samsung.SMT
@REM # Samsungs (way worse) Google Ripoffs
adb shell pm enable com.samsung.android.aremojieditor
adb shell pm enable com.samsung.android.ipsgeofence
adb shell pm enable com.sec.android.app.popupcalculator
adb shell pm enable com.samsung.android.accessibility.talkback
adb shell pm enable com.samsung.android.messaging
adb shell pm enable com.samsung.android.calendar
adb shell pm enable com.samsung.android.app.contacts
adb shell pm enable com.samsung.android.app.reminder
adb shell pm enable com.samsung.android.da.daagent
adb shell pm enable com.samsung.android.dialer
adb shell pm enable com.samsung.android.forest
adb shell pm enable com.samsung.android.app.notes
adb shell pm enable com.samsung.android.game.gamehome
adb shell pm enable com.samsung.android.game.gametools
adb shell pm enable com.samsung.android.game.gos
adb shell pm enable com.samsung.android.gametuner.thin
adb shell pm enable com.samsung.android.video
adb shell pm enable com.samsung.android.service.health
adb shell pm enable com.sec.android.app.clockpackage
adb shell pm enable com.sec.android.app.myfiles
@REM adb shell pm enable com.sec.android.gallery3d
adb shell pm enable com.samsung.android.scloud
adb shell pm enable com.samsung.android.smartswitchassistant
adb shell pm enable com.samsung.android.smartmirroring
adb shell pm enable com.sec.android.smartfpsadjuster
adb shell pm enable com.samsung.android.smartsuggestions
adb shell pm enable com.samsung.android.bbc.bbcagent

@REM ## ###########
@REM #
@REM # 3rd Party Bloatware to Enable
@REM #
@REM #####
adb shell pm enable com.amazon.fv
adb shell pm enable com.amazon.kindle
adb shell pm enable com.amazon.mp3
adb shell pm enable com.amazon.mShop.android
adb shell pm enable com.amazon.venezia
adb shell pm enable flipboard.boxer.app
adb shell pm enable com.facebook.appmanager
adb shell pm enable com.facebook.katana
adb shell pm enable com.facebook.services
adb shell pm enable com.facebook.system
adb shell pm enable com.microsoft.appmanager
adb shell pm enable com.microsoft.skydrive
adb shell pm enable com.microsoft.office.outlook
adb shell pm enable com.microsoft.office.officehubrow
adb shell pm enable com.microsoft.teams
adb shell pm enable de.axelspringer.yana.zeropage

@REM ## ###########
@REM #
@REM # Ask to Reboot
@REM #
@REM #####
setlocal
:PROMPT
SET /P REBOOTNOW=Reboot now (Y/[N])?
IF /I "%REBOOTNOW%" NEQ "Y" GOTO END
adb reboot
:END
