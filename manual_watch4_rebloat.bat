@REM ## ###########
@REM #
@REM # Samsung Bloatware to Enable
@REM # thanks @ https://forum.xda-developers.com/t/debloat-galaxy-watch-4.4324147/
@REM #
@REM #####

@REM # Samsung Account
adb shell pm enable com.samsung.android.watch.watchface.myphoto
adb shell pm enable com.samsung.android.watch.watchface.mystyle
adb shell pm enable com.samsung.android.watch.compass
adb shell pm enable com.samsung.android.watch.weather
adb shell pm enable com.samsung.android.messaging
adb shell pm enable com.samsung.android.watch.watchface.basicclock
adb shell pm enable com.samsung.android.bixby.agent
adb shell pm enable com.samsung.android.watch.watchface.simpleclassic
adb shell pm enable com.samsung.android.watch.watchface.dualwatch
adb shell pm enable com.samsung.android.samsungpay.gear
adb shell pm enable com.samsung.android.app.reminder
adb shell pm enable com.samsung.android.watch.watchface.together
adb shell pm enable com.samsung.android.watch.watchface.typography
adb shell pm enable com.samsung.android.watch.watchface.large
adb shell pm enable com.samsung.android.watch.watchface.animal
adb shell pm enable com.google.android.apps.maps
adb shell pm enable com.samsung.android.watch.watchface.aremoji
adb shell pm enable com.samsung.android.watch.watchface.analogmodular
adb shell pm enable com.samsung.android.watch.watchface.emergency
adb shell pm enable com.samsung.android.watch.watchface.simpleanalogue
adb shell pm enable com.google.android.apps.wearable.retailattractloop
adb shell pm enable com.samsung.android.bixby.wakeup
adb shell pm enable com.samsung.android.watch.watchface.bitmoji
adb shell pm enable com.android.providers.userdictionary
adb shell pm enable com.samsung.android.watch.watchface.superfiction
adb shell pm enable com.samsung.android.watch.worldclock
adb shell pm enable com.samsung.android.watch.watchface.endangeredanimal

adb shell pm enable com.samsung.android.shealthmonitor

adb shell pm enable com.samsung.android.watch.watchface.analoguefont
adb shell pm enable com.samsung.android.watch.worldclock
adb shell pm enable com.samsung.android.watch.cameracontroller
adb shell pm enable com.samsung.android.watch.watchface.simplecomplication
adb shell pm enable com.samsung.android.watch.watchface.digitalfont
adb shell pm enable com.samsung.android.watch.watchface.digitalmodular
adb shell pm enable com.google.android.marvin.talkback
adb shell pm enable com.samsung.android.wearable.music

adb shell pm enable com.samsung.android.watch.watchface.weather
adb shell pm enable com.samsung.android.watch.watchface.healthmodular
adb shell pm enable com.samsung.android.watch.watchface.livewallpaper

adb shell pm enable com.google.android.wearable.assistant

adb shell pm enable com.samsung.android.watch.watchface.basicdashboard
adb shell pm enable com.samsung.android.video.wearable

@REM ## ###########
@REM #
@REM # Ask to Reboot
@REM #
@REM #####
setlocal
:PROMPT
SET /P REBOOTNOW=Reboot now (Y/[N])?
IF /I "%REBOOTNOW%" NEQ "Y" GOTO END
adb reboot
:END
