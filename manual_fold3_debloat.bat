@REM ## ###########
@REM #
@REM # Samsung Bloatware to Disable
@REM #
@REM #####

@REM # Bixby
adb shell pm disable-user --user 0 com.samsung.android.bixby.wakeup
adb shell pm disable-user --user 0 com.samsung.android.app.spage
adb shell pm disable-user --user 0 com.samsung.android.app.routines
adb shell pm disable-user --user 0 com.samsung.android.bixby.service
adb shell pm disable-user --user 0 com.samsung.android.visionintelligence
adb shell pm disable-user --user 0 com.samsung.android.bixby.agent
adb shell pm disable-user --user 0 com.samsung.android.bixby.agent.dummy
adb shell pm disable-user --user 0 com.samsung.android.bixbyvision.framework
adb shell pm disable-user --user 0 com.samsung.android.app.settings.bixby
adb shell pm disable-user --user 0 com.samsung.systemui.bixby
adb shell pm disable-user --user 0 com.samsung.systemui.bixby2
@REM # Samsung Account
@REM adb shell pm disable-user --user 0 com.osp.app.signin
@REM # Samsung Auto fill
adb shell pm disable-user --user 0 com.samsung.android.samsungpassautofill
@REM # Samsung Authentication
adb shell pm disable-user --user 0 com.samsung.android.authfw
@REM # Samsung Pass
adb shell pm disable-user --user 0 com.samsung.android.samsungpass
@REM # Samsung Pay
adb shell pm disable-user --user 0 com.samsung.android.spay
adb shell pm disable-user --user 0 com.samsung.android.spayfw
adb shell pm disable-user --user 0 com.sec.android.app.billing
@REM # Samsung Email
adb shell pm disable-user --user 0 com.samsung.android.email.provider
adb shell pm disable-user --user 0 com.wsomacp
@REM # Samsung Internet
adb shell pm disable-user --user 0 com.sec.android.app.sbrowser
@REM # Samsung Blockchain Wallet
adb shell pm disable-user --user 0 com.samsung.android.coldwalletservice
@REM # Samsung Text-to-Speech
adb shell pm disable-user --user 0 com.samsung.SMT
@REM # Samsungs (way worse) Google Ripoffs
adb shell pm disable-user --user 0 com.samsung.android.aremojieditor
adb shell pm disable-user --user 0 com.samsung.android.ipsgeofence
adb shell pm disable-user --user 0 com.sec.android.app.popupcalculator
adb shell pm disable-user --user 0 com.samsung.android.accessibility.talkback
adb shell pm disable-user --user 0 com.samsung.android.messaging
adb shell pm disable-user --user 0 com.samsung.android.calendar
adb shell pm disable-user --user 0 com.samsung.android.fmm
adb shell pm disable-user --user 0 com.samsung.android.mdx
adb shell pm disable-user --user 0 com.samsung.android.app.contacts
adb shell pm disable-user --user 0 com.samsung.android.app.reminder
adb shell pm disable-user --user 0 com.samsung.android.da.daagent
adb shell pm disable-user --user 0 com.samsung.android.dialer
adb shell pm disable-user --user 0 com.samsung.android.forest
adb shell pm disable-user --user 0 com.samsung.android.app.notes
adb shell pm disable-user --user 0 com.samsung.android.app.notes.addons
adb shell pm disable-user --user 0 com.samsung.android.game.gamehome
adb shell pm disable-user --user 0 com.samsung.android.game.gametools
adb shell pm disable-user --user 0 com.samsung.android.game.gos
adb shell pm disable-user --user 0 com.samsung.android.gametuner.thin
adb shell pm disable-user --user 0 com.samsung.android.video
adb shell pm disable-user --user 0 com.samsung.android.service.health
adb shell pm disable-user --user 0 com.sec.android.app.clockpackage
adb shell pm disable-user --user 0 com.sec.android.app.myfiles
@REM adb shell pm disable-user --user 0 com.sec.android.gallery3d
adb shell pm disable-user --user 0 com.samsung.android.scloud
adb shell pm disable-user --user 0 com.samsung.android.smartswitchassistant
adb shell pm disable-user --user 0 com.samsung.android.smartmirroring
adb shell pm disable-user --user 0 com.sec.android.smartfpsadjuster
adb shell pm disable-user --user 0 com.samsung.android.smartsuggestions
adb shell pm disable-user --user 0 com.samsung.android.bbc.bbcagent

@REM ## ###########
@REM #
@REM # 3rd Party Bloatware to Disable
@REM #
@REM #####
adb shell pm disable-user --user 0 com.amazon.fv
adb shell pm disable-user --user 0 com.amazon.kindle
adb shell pm disable-user --user 0 com.amazon.mp3
adb shell pm disable-user --user 0 com.amazon.mShop.android
adb shell pm disable-user --user 0 com.amazon.venezia
adb shell pm disable-user --user 0 flipboard.boxer.app
adb shell pm disable-user --user 0 com.facebook.appmanager
adb shell pm disable-user --user 0 com.facebook.katana
adb shell pm disable-user --user 0 com.facebook.services
adb shell pm disable-user --user 0 com.facebook.system
adb shell pm disable-user --user 0 com.microsoft.appmanager
adb shell pm disable-user --user 0 com.microsoft.skydrive
adb shell pm disable-user --user 0 com.microsoft.office.outlook
adb shell pm disable-user --user 0 com.microsoft.office.officehubrow
adb shell pm disable-user --user 0 com.microsoft.teams
adb shell pm disable-user --user 0 de.axelspringer.yana.zeropage

@REM ## ###########
@REM #
@REM # Ask to Reboot
@REM #
@REM #####
setlocal
:PROMPT
SET /P REBOOTNOW=Reboot now (Y/[N])?
IF /I "%REBOOTNOW%" NEQ "Y" GOTO END
adb reboot
:END
